/*
 * Copyright (c) 2016 Glenn Wurr III <glenn@wurr.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "main.h"


SDL_Surface *screen, *temp, *grass;

snake players[MAX_PLAYERS]; // arroy of all players
snake *player = &players[0]; // pointer to the current player


void init_sprites(void){

	/* load grass */
	temp  = SDL_LoadBMP("grass.bmp");
	grass = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

}

void init_players(void){
	for (int a = 0; a < MAX_PLAYERS ; a++) {
		/* set sprite position */
		players[a].x = ( rand() % SCREEN_WIDTH ) + 1;
		players[a].y = ( rand() % SCREEN_HEIGHT ) + 1;
		players[a].color = ( rand() % 8 ) + 1;
		players[a].score = 0;
		players[a].length = 0;
		players[a].width = SNAKE_START_WIDTH;
		players[a].grow = SNAKE_START_LENGTH;
		players[a].dead = 0;
		players[a].direction = ( rand() % 4 ) + 1;
	}
}


int blit_surface(SDL_Surface *src, SDL_Surface *dest, Sint16 x, Sint16 y) {

	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	return SDL_BlitSurface(src, NULL, dest, &offset);
}

void draw_bg(SDL_Surface *src, SDL_Surface *dest) {

	int offx, offy;
		 /* draw the grass */
		for (int x = 0; x < SCREEN_WIDTH/BG_SPRITE_SIZE; x++) {
			for (int y = 0; y < SCREEN_HEIGHT/BG_SPRITE_SIZE; y++) {
				offx = x * BG_SPRITE_SIZE;
				offy = y * BG_SPRITE_SIZE;
				blit_surface(src, dest, offx, offy);
			}
		}

}

void redraw_all_images(void) {

	draw_bg(grass, screen);

	for (int a = 0; a < MAX_PLAYERS ; a++) {
		if (!players[a].dead) { // dont draw dead players
			draw_square(screen, players[a].x, players[a].y, players[a].width, players[a].color);
			for (int b = 0; b < players[a].length ; b++) {
				draw_square(screen, players[a].tail[b].x, players[a].tail[b].y, players[a].width, players[a].color);
			}
		}
	}
	/* update the screen */
	SDL_UpdateRect(screen,0,0,0,0);
}

void cleanup_sdl_stuff(void){

	SDL_FreeSurface(grass);
	SDL_Quit();
}


void game_tick(void){
	for (int a = 0; a < MAX_PLAYERS ; a++) {
		movesnake(&players[a]);
	}
}


void handle_input(Uint8 *keystate){
	if (!player->dead) {
			if (keystate[SDLK_LEFT] ) {
				player->direction = 4;
			}else if (keystate[SDLK_RIGHT] ) {
				player->direction = 2;
			}else if (keystate[SDLK_UP] ) {
				player->direction = 1;
			}else if (keystate[SDLK_DOWN] ) {
				player->direction = 3;
			}else if (keystate[SDLK_SPACE]) {
				growsnake(player, 1);
			}
	}
}

void main_game_loop(void) {
	SDL_Event event;
	Uint8 *keystate;
	Uint32 start_ticks = 0, end_ticks = 0;


	int gameover = 0;

	/* message pump */
	while (!gameover)
	{
		/* look for an event */
		if (SDL_PollEvent(&event)) {
			/* an event was found */
			switch (event.type) {
				/* close button clicked */
				case SDL_QUIT:
					gameover = 1;
					break;

				/* handle the keyboard */
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
						case SDLK_ESCAPE:
						case SDLK_q:
							gameover = 1;
							break;
						default:
							continue;
					}
					break;
			}
		}

		keystate = SDL_GetKeyState(NULL);
		handle_input(keystate);
		game_tick();

		start_ticks = SDL_GetTicks();
		redraw_all_images();
		end_ticks = SDL_GetTicks();
		cap_fps(start_ticks, end_ticks);


	}


}


int main(int argc, char* argv[])
{


	srand(time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO) < 0 ) return 1;

	SDL_WM_SetCaption("SNEK", "SNEK");

	//if (!(screen = SDL_SetVideoMode(WIDTH, HEIGHT, DEPTH, SDL_FULLSCREEN|SDL_HWSURFACE))) // fullscreen
	if (!(screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_DEPTH, SDL_HWSURFACE))) //not fullscreen
	{
		SDL_Quit();
		fprintf(stderr,"COULD NOT CREATE SCREEN\n");
		return 1;
	}

	init_sprites();
	init_players();



	main_game_loop();
	// this will block until game is over

	/* clean up */
	cleanup_sdl_stuff();


	return 0;
}
