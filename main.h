/*
 * Copyright (c) 2016 Glenn Wurr III <glenn@wurr.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>


#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 640
#define SCREEN_BPP 4
#define SCREEN_DEPTH 8
#define BG_SPRITE_SIZE 32
#define FRAMES_PER_SECOND 30

#define MAX_PLAYERS 4
#define MOVE_SPEED 1
#define MAX_TAIL 4096

#define SNAKE_LEFT 4
#define SNAKE_RIGHT 2
#define SNAKE_UP 1
#define SNAKE_DOWN 3
#define SNAKE_START_WIDTH 5
#define SNAKE_START_LENGTH 50

typedef struct tailsegment {
	int x, y;
} tailsegment;

typedef struct snake {
	int x, y;
	int score;
	int length;
	int width;
	int grow;
	tailsegment tail[MAX_TAIL];
	int direction;
	Uint8 color;
	int dead;
} snake;

void growsnake(snake *player, int i) {
	player->grow = player->width * i;
}



void movesnake(snake *player) {
	// LEFT
	if (player->direction == SNAKE_LEFT) {
		player->x -= MOVE_SPEED;
	}
	//RIGHT
	if (player->direction == SNAKE_RIGHT) {
		player->x += MOVE_SPEED;
	}
	
	// UP
	if (player->direction == SNAKE_UP) {
		player->y -= MOVE_SPEED;
	}
	// DOWN
	if (player->direction == SNAKE_DOWN) {
		player->y += MOVE_SPEED;
	}
	
	if (player->length != 0 || player->grow > 0) {
		player->tail[player->length].x = player->x;
		player->tail[player->length].y = player->y;
		player->length++;
		if (!player->grow){
			player->length--;
			memmove(player->tail, &player->tail[1], player->length * sizeof player->tail[0]);
		} else {
			player->grow--;
		}
	}
	if ( player->length >= 5 ) {
		player->width =  (int)( sqrt((float)player->length) / 2.0 ) ;
	}
	
	/* collide with edges of screen */
	if ( player->x < 0 ) {
		player->x = 0;
		player->dead = 1;
	}
	else if ( player->x > SCREEN_WIDTH - player->width ) {
		player->x = SCREEN_WIDTH - player->width;
		player->dead = 1;
	}
	if ( player->y < 0 ) {
		player->y = 0;
		player->dead = 1;
	}
	else if ( player->y > SCREEN_HEIGHT - player->width ) {
		player->y = SCREEN_HEIGHT - player->width;
		player->dead = 1;
	}

}



void cap_fps(Uint32 start_ticks, Uint32 end_ticks) {

	int sleep_delay = (1000 / FRAMES_PER_SECOND) - (end_ticks-start_ticks);

	if (sleep_delay > 0) {
		SDL_Delay(sleep_delay);
	}
}

void draw_square(SDL_Surface *screen, int x, int y, int width, Uint8 color) {

	int i, n;

	Uint8 *p = screen->pixels;
	p += x + y * screen->w;

	for(n = 0; n < width; n++) {
		for(i = 0; i < width; i++)
			*p++ = color;
		p += screen->w - width;
	}

}
