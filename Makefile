CFLAGS=-Wall -std=c11 -O3

ifeq ($(OS),Windows_NT)
	# an example for when you might be having path issues
	#CC=C:\msys64\mingw64\bin\gcc.exe
	#CFLAGS+=-I/mingw64/include/SDL -Dmain=SDL_main
	#LIBS=-L/mingw64/lib -lmingw32 -lSDLmain -lSDL -mwindows

	CC=gcc
	CFLAGS+=-Dmain=SDL_main
	LIBS=-lmingw32 -lSDLmain -lSDL -mwindows
else
	CFLAGS+=
	LIBS=-lSDL -lm
	CC=cc
endif

all: clean game-build

game-build: main.c
	@echo ${CC} ${CFLAGS} main.c -o game ${LIBS}
	@${CC} ${CFLAGS} main.c -o game ${LIBS}

clean:
	@echo cleaning
	@rm -f game
	@rm -f game.exe
